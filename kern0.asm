
kern0:     file format elf32-i386


Disassembly of section .text:

00100000 <multiboot>:
  100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
  100006:	00 00                	add    %al,(%eax)
  100008:	fe 4f 52             	decb   0x52(%edi)
  10000b:	e4                   	.byte 0xe4

0010000c <vga_write>:
// volatile static unsigned *VGABUF = ((volatile unsigned *) 0xb8000);

static const char XMAX = 80;
static const char YMAX = 25;

static void vga_write(const char *s, int8_t linea, uint8_t color) { 
  10000c:	53                   	push   %ebx
        volatile char *buf = VGABUF;
        if (linea < YMAX || linea > -YMAX){ 
            char offset = 0;
            if (linea < 0) {
                offset = YMAX; 
  10000d:	89 d3                	mov    %edx,%ebx
  10000f:	c0 fb 07             	sar    $0x7,%bl
            }    
            buf += XMAX*2*(offset+linea);
  100012:	83 e3 19             	and    $0x19,%ebx
  100015:	0f be d2             	movsbl %dl,%edx
  100018:	01 da                	add    %ebx,%edx
  10001a:	8d 14 92             	lea    (%edx,%edx,4),%edx
  10001d:	c1 e2 05             	shl    $0x5,%edx
  100020:	81 c2 00 80 0b 00    	add    $0xb8000,%edx
            while (*s != 0)
  100026:	0f b6 18             	movzbl (%eax),%ebx
  100029:	84 db                	test   %bl,%bl
  10002b:	74 12                	je     10003f <vga_write+0x33>
            {
                *buf++ = *s++;
  10002d:	83 c0 01             	add    $0x1,%eax
  100030:	88 1a                	mov    %bl,(%edx)
                *buf++ = color;
  100032:	88 4a 01             	mov    %cl,0x1(%edx)
            while (*s != 0)
  100035:	0f b6 18             	movzbl (%eax),%ebx
                *buf++ = color;
  100038:	8d 52 02             	lea    0x2(%edx),%edx
            while (*s != 0)
  10003b:	84 db                	test   %bl,%bl
  10003d:	75 ee                	jne    10002d <vga_write+0x21>
            }
        }
    }
  10003f:	5b                   	pop    %ebx
  100040:	c3                   	ret    

00100041 <comienzo>:

void comienzo(void) {
  100041:	83 ec 10             	sub    $0x10,%esp
    // *buf++ = 47;
    // *buf++ = 75;
    // *buf++ = 47;

    // volatile char * buf = VGABUF;
    char s[] = "OK";
  100044:	66 c7 44 24 0d 4f 4b 	movw   $0x4b4f,0xd(%esp)
  10004b:	c6 44 24 0f 00       	movb   $0x0,0xf(%esp)
    vga_write(s, 0, 47);
  100050:	8d 44 24 0d          	lea    0xd(%esp),%eax
  100054:	b9 2f 00 00 00       	mov    $0x2f,%ecx
  100059:	ba 00 00 00 00       	mov    $0x0,%edx
  10005e:	e8 a9 ff ff ff       	call   10000c <vga_write>
    char s2[] = "HOLA";
  100063:	c7 44 24 08 48 4f 4c 	movl   $0x414c4f48,0x8(%esp)
  10006a:	41 
  10006b:	c6 44 24 0c 00       	movb   $0x0,0xc(%esp)
    vga_write(s2, 1, 224);
  100070:	8d 44 24 08          	lea    0x8(%esp),%eax
  100074:	b9 e0 00 00 00       	mov    $0xe0,%ecx
  100079:	ba 01 00 00 00       	mov    $0x1,%edx
  10007e:	e8 89 ff ff ff       	call   10000c <vga_write>
    while (1)
        asm("hlt");
  100083:	f4                   	hlt    
  100084:	eb fd                	jmp    100083 <comienzo+0x42>
