
# Kernel mínimo en C

## **Compilar un kernel y lanzarlo en QEMU**
## **Ej: kern0-boot**

### ¿Emite algún aviso el proceso de compilado o enlazado? Si lo hubo, indicar cómo usar la opción --entry de ld(1) para subsanarlo.

>Al enlazar usando el flag `-Ttext 0x10000`, se le indica al linker el inicio de la seccion _start, pero al esta no estar definida en el codigo, avisa que no se la encuentra. 
Para evitar este problema se puede o bien redefinir la funcion comienzo como `_start` o pasarle la opción `--entry` con la dirección de inicio de ejecución del programa.  


### ¿Cuánta CPU consume el proceso qemu-system-i386 mientras ejecuta este kernel? ¿Qué está haciendo? 

>El proceso consume 100% del cpu, esta todo el tiempo corriendo el bucle `while(1){continue;}`, sin terminar nunca el programa.

## **Ej: kern0-quit**

### Resultado de `info registers`

```
EAX=2badb002 EBX=00009500 ECX=00100000 EDX=00000511
ESI=00000000 EDI=0804a000 EBP=00000000 ESP=00006f08
EIP=00100001 EFL=00000006 [-----P-] CPL=0 II=0 A20=1 SMM=0 HLT=1
ES =0010 00000000 ffffffff 00cf9300 DPL=0 DS   [-WA]
CS =0008 00000000 ffffffff 00cf9a00 DPL=0 CS32 [-R-]
SS =0010 00000000 ffffffff 00cf9300 DPL=0 DS   [-WA]
DS =0010 00000000 ffffffff 00cf9300 DPL=0 DS   [-WA]
FS =0010 00000000 ffffffff 00cf9300 DPL=0 DS   [-WA]
GS =0010 00000000 ffffffff 00cf9300 DPL=0 DS   [-WA]
LDT=0000 00000000 0000ffff 00008200 DPL=0 LDT
TR =0000 00000000 0000ffff 00008b00 DPL=0 TSS32-busy
GDT=     000caa68 00000027
IDT=     00000000 000003ff
CR0=00000011 CR2=00000000 CR3=00000000 CR4=00000000
DR0=00000000 DR1=00000000 DR2=00000000 DR3=00000000 
DR6=ffff0ff0 DR7=00000400
EFER=0000000000000000
FCW=037f FSW=0000 [ST=0] FTW=00 MXCSR=00001f80
FPR0=0000000000000000 0000 FPR1=0000000000000000 0000
FPR2=0000000000000000 0000 FPR3=0000000000000000 0000
FPR4=0000000000000000 0000 FPR5=0000000000000000 0000
FPR6=0000000000000000 0000 FPR7=0000000000000000 0000
XMM00=00000000000000000000000000000000 XMM01=00000000000000000000000000000000
XMM02=00000000000000000000000000000000 XMM03=00000000000000000000000000000000
XMM04=00000000000000000000000000000000 XMM05=00000000000000000000000000000000
XMM06=00000000000000000000000000000000 XMM07=00000000000000000000000000000000
```

## **Ej: kern0-hlt**

### ¿Una vez invocado hlt ¿cuándo se reanuda la ejecución?

>La ejecución es reanudada cuando se le envía al procesador una interrupcion externa, tal como la señal de un timer o una señal envíada por un dispositivo como un teclado.

EVENTS WITH ASM
            
 TYPE |Usage       |Events/s    |Category       |Description
 ---|---|---|---|---
with `hlt` |    7,0 ms/s  |    36,8   |     Process        [PID 17261] | qemu-system-i386 -serial mon:stdio -kernel kern0 -gdb tcp:127.0.0.1:7508
without `hlt` | 553,8 ms/s    |  60,6  |      Process        [PID 19251] |qemu-system-i386 -serial mon:stdio -kernel kern0 -gdb tcp:127.0.0.1:7508

            
## **Ej. kern0-gdb**


### ¿Por qué hace falta el modificador /x al imprimir %eax, y no así %esp?

>Porque %eax siempre tiene la direccion de un puntero, %esp podria tener un valor distinto.

### ¿Qué significado tiene el valor que contiene %eax, y cómo llegó hasta ahí? (Revisar la documentación de Multiboot, en particular la sección Machine state.)

>El valor del campo `$eax` le indica al S.O. que fue cargado usando un boot loader conforme al estandar Multiboot. Se le fue indicado en el archivo boot.S 

variable | valor
--- | ---
`$esp`| 0x6f08
`$eax` | 0x2badb002


```gdb
(gdb) x/4xw $ebx
0x9500: 0x0000024f      0x0000027f      0x0001fb80      0x8000ffff
```
El comando `x/4xw` imprime desde la posicion apuntada por `$ebx` en formato hexadecimal, las primeras 4 words.

* La primera word es el campo de `flags`, que indica que valores son validos de la estructura de información de multiboot. 
* La segunda y tercera word son los campos `mem_lower` y `mem_upper`, indicando la cantidad de memoria, el primer campo en Kb y el segundo en Mb
* La cuarta word consiste en el `boot_device`, indicando desde donde se carga la imaen del OS. En este caso se carga desde el primer disco duro, por el `0x80`, con el numero de particion 0, consistente con un nivel de particionamiento simple DOS, con los campos part2 y part3 valiendo `0xff`
  
  
El flag 0  esta encendido, por lo que es válido

valor | resultado | comando 
--- | ---  | ---
`flags`  | 0000001001001111 | `x/1th $ebx`
`lower memory` | 639 KB | `x/1dh $ebx + 4`
`cmdline` | " " | `x/4sw *($ebx + 16)`
`boot_loader_name` | " " | ``x/4sw *($ebx + 64)``
  `higher memory`| 129920 MB | `x/1dw $ebx+8; p $__`

---
## Makefile y flags de compilación
## **Ej: make-flags**

### ¿Qué compilador usa make por omisión? ¿Es o no gcc? Explicar cómo se podría forzar el uso de clang:

>Por omisión make usa cc, que en el sistema es un link a gcc. Para forzar el uso de clang se puede o pasar la opcion `make CC=clang` para una única vez, o agregar CC = clang en el makefile para todas las compilaciones del proyecto, o cambiar el link de cc a clang para un seteo global.

### ¿Se pueden usar booleanos en modo “free standing”?

>Si, el modo freestanding incluye el header de stdbool.h, donde se define el tipo de dato booleano y sus valores de true y false  (`man stdbool.h`) 

### ¿Dónde se definen los tipos uint8_t, int32_t, etc.?

>Se definen en el header stdint.h, donde tambien se definen los limites maximos y minimos de cada tipo de dato

### Escribir un archivo c99int.h con las directivas typedef necesarias para definir tipos enteros propios de 8, 16, 32 y 64 bits, con signo y sin signo.


## **Ej: make-pattern**


### ¿Cómo funciona la regla que compila boot.S a boot.o?
> La regla es `$(CC) $(CFLAGS) -c $<`, funciona usando el compilador definido por `CC` y las flags `CFLAGS`, agregandole el flag `-c` y usando la variable automatica que contiene el archivo fuente.

### ¿Qué son las variables $@, $^ y $<?

> * La variable `$@` contiene el nombre del archivo de destino, en este caso `kern0`. 
> * La variable `$^` contiene los nombres de todos los prerequisitos necesarios para la regla del make, en este caso la lista de archivos "*.o" usados por el linker
> * La variable `$<` contiene el nombre del primer prerequisito de la regla, en este ejemplo es el nombre del archivo "*.S"

    
### La regla kern0 usa $^ y la regla con %.S usa $<.    ¿Qué ocurriría si se intercambiaran estas dos variables entre ambas reglas?

> Para la regla que tiene `%.S` no habria problema, ya que es un solo archivo el que se ensambla a un archivo objeto, pero para la regla de kern0 no incluiria al archivo `kern0.o`, ya que `$<` incluye solo al primero

## **Ej: make-implicit**

### ¿Mediante qué regla se genera el archivo kern0.o?
> Usando la regla implicita  `$(CC) $(CPPFLAGS) $(CFLAGS) -c` con todos los archivos `*.c` que existan en el directorio donde fue ejecutado make 
    
### Eliminar la regla %.o: %.S y ejecutar make clean kern0: 
* ### ¿Se llega a generar el archivo boot.o?
  
  > El archivo boot.o se genera, pero ocurre un error de incompatibilidad, el archivo objeto no se ensamblo con los flags apropiados "-m32" y como el sistema es 64 bit no puede linkear correctamente

* ### ¿Ocurre algún otro error? (Si no ocurre, mostrar la salida del comando uname -m).
  > `x86_64`

* ### ¿Se puede subsanar el error sin re-introducir la regla eliminada?
  > Usando la regla implicita de ensamblar archivos .S a .s, se le agrega el flag `CPPFLAGS="-m32"` como argumento de make 

---
## El buffer VGA
## **Ej: kern0-vga**



### Explicar el código

> Primero se define un puntero al inicio de la memoria de texto de video, siendo esta `0xB8000`. Despues se escriben los caracteres en la pantalla, componiendose de 2 bytes. El primer byte es el codigo ASCII, y el segundo es el atributo del caracter, donde los 4 bits mas bajos corresponden al color del caracter y los 3 siguientes son el color del fondo. Asi, se imprimen los caracteres OK
> Position | ASCI | Char | Attribute | Attr (HEX) | Foreground | Background
> --- | ---| ---|---|---|--- | ---
> 0xb8000 | 79 | O | 47 | 0x2F | F | 2
> 0xb8002 | 75 | K | 47 | 0x2F | F | 2
> El modificador volatile se usa debido a que la posicion de memoria puede cambiar por algun otro programa, ya que donde se escribe texto. Si no estuviera, el compilador podria interpretar que es el unico que escribe en ese sector de memoria y podria cachear los resultados en algun registro, por lo tanto al leer desde esa posición daria un valor invalido. Volatile le obliga a leer el valor real cada vez que se lo accede.

#### Funcion 
```c
static void 
vga_write(const char *s, int8_t linea, uint8_t color) { 
        volatile char *buf = VGABUF;
        if (linea < YMAX || linea > -YMAX){ 
            char offset = 0;
            if (linea < 0) {
               offset = YMAX; 
            }    
            buf += XMAX*2*(offset+linea);
            while (*s != 0)
            {
                *buf++ = *s++;
                *buf++ = color;
            }
        }
    }
```


## **Ej: kern0-const**

### 1. Explicar los errores o avisos de compilación que ocurren al recompilar el código original con la nueva definición.

> Usando el modificador const en la definicion de VGABUF se genera un conflicto con las definiciones de variables *buf para la función comienzo, donde esta no es const. Para corregir este error se le quita el modificador const a `VGABUF`

### 2. La declaración de VGABUF resultante del punto anterior ¿permite avanzar directamente la variable global? ¿Qué ocurre al añadir la siguiente línea a la función comienzo?
>  Si, le permite dar un offset, ya que modifica al puntero directamente. Sin embargo, si se le agrega `static char * const VGABUF`, este codigo no compila ya que no puedo modificar VGABUF directamente, pero si creo otra variable como *buf que tenga el valor de VGABUF entonces si la puedo modificar

### 3. ¿Se podría cambiar el tipo de VGABUF para que no se permita el uso directo de VGABUF?

> Al ejecutar el codigo dado aparece una x, ya que se le asigna a la posicion de memoria de VBABUF + 120 bytes, 60 caracteres a la derecha, el caracter ascii de 88 mas lo que contenia anteriormente. Si VGABUF toma el tipo const, entonces no es posible la modificacion de lo que apunta, pero si la de su posicion.

## **Ej: kern0-endian**

### 1. Compilar el siguiente programa y justificar la salida que produce en la terminal.

> el numero 57616 en hexadecimal es `e110`, por lo tanto se estan imprimiendo los 4 numeros hexa en ese orden. Para el segundo, se aprovecha de que la arquitectura es little endian, ya que el numero que esta guardado con el byte 0 en primera posición. De esta manera, al leer el valor de un puntero, y este guardandose como little endian, aparece con el orden de los bytes inversos.
Para que en una maquina que guarda los datos en formato big endian, el valor i deberia ser `unsigned int i = 0x726c6400;`

```c
volatile static unsigned *VGABUF = ((volatile unsigned *) 0xb8000);

void comienzo(void) {
    volatile unsigned *p = VGABUF;
    *p = 0x2F4B2F4F;

    while (1)
        asm("hlt");
}
```




### 3. Usar un puntero a uint64_t para imprimir en la segunda línea de la pantalla la palabra HOLA, en negro sobre amarillo.

```c
// Versión 1
volatile uint64_t *p = VGABUF + 160;
*p = 0xE041E04cE04fE048

// Versión 2
volatile uint64_t *p = VGABUF;
p += 160;
*p = 0xE041E04cE04fE048
```
> La diferencia entre ambas versiones implica la posicion de memoria que se guarda en p al momento de inicializar. En el primer caso es la posicion VBABUF + 160 bytes, pero en el segundo se le suma 160 tamaños de la palabra, y al ser esta de 8 bytes, se imprime la linea 8 lineas mas abajo en vez de una.



## **Ej: kern0-objdump**

### 2. ¿En qué cambia el código generado si se recompila con la opción -fno-inline de GCC? 
> La rutina `vga_write`, que antes por optimizaciones se ponía dentro del mismo codigo de comienzo, al darle la opción -fno-inline, se genera el codigo assembler en una seccion separada, pudiendo interpretarla mejor.

### 3. Sustituir la opción -d de objdump por -S, y explicar las diferencias en el resultado.

> Al sustituir el flag -S se genera un intercalado del codigo C original y el codigo assembler que se genera. el flag -S implica el flag -d

### 4. De la salida de objdump -S sobre el binario compilado con -fno-inline, incluir la sección correspondiente a la función vga_write() y explicar cada instrucción de assembler en relación al código C original.

```c
static void vga_write(const char *s, int8_t linea, uint8_t color) { 
  10000c:	53                   	push   %ebx
        volatile char *buf = VGABUF;
        if (linea < YMAX || linea > -YMAX){ 
            char offset = 0;
            if (linea < 0) {
               offset = YMAX; 
  10000d:	89 d3                	mov    %edx,%ebx
  10000f:	c0 fb 07             	sar    $0x7,%bl
            }    
            buf += XMAX*2*(offset+linea);
  100012:	83 e3 19             	and    $0x19,%ebx
  100015:	0f be d2             	movsbl %dl,%edx
  100018:	01 da                	add    %ebx,%edx
  10001a:	8d 14 92             	lea    (%edx,%edx,4),%edx
  10001d:	c1 e2 05             	shl    $0x5,%edx
  100020:	81 c2 00 80 0b 00    	add    $0xb8000,%edx
            while (*s != 0)
  100026:	0f b6 18             	movzbl (%eax),%ebx
  100029:	84 db                	test   %bl,%bl
  10002b:	74 12                	je     10003f <vga_write+0x33>
            {
                *buf++ = *s++;
  10002d:	83 c0 01             	add    $0x1,%eax
  100030:	88 1a                	mov    %bl,(%edx)
                *buf++ = color;
  100032:	88 4a 01             	mov    %cl,0x1(%edx)
            while (*s != 0)
  100035:	0f b6 18             	movzbl (%eax),%ebx
                *buf++ = color;
  100038:	8d 52 02             	lea    0x2(%edx),%edx
            while (*s != 0)
  10003b:	84 db                	test   %bl,%bl
  10003d:	75 ee                	jne    10002d <vga_write+0x21>
            }
        }
    }
  10003f:	5b                   	pop    %ebx
  100040:	c3                   	ret    

```
%ecx tiene al color, %eax tiene a s
* `push   %ebx`   : Guarda el contenido de ebx en el stack pointer
* `mov    %edx,%ebx` : Copia el contenido de edx a ebx (linea)
* `sar    $0x7,%bl` : Hace un shift aritmetico de 7 lugares al ultimo byte de %ebx, para ver si es menor a cero, de ser asi quedan todos en 1
* `and    $0x19,%ebx` : Setea el valor 25 en el %ebx si era negativo, sino queda en cero
* `movsbl %dl,%edx` : Extiende el valor de dl, que es de un byte, a doble palabra, con extension de signo
* `add    %ebx,%edx` : Suma %ebx a %edx, que era el offset
* `lea    (%edx,%edx,4),%edx` : carga en edx la posicion de 
* `shl    $0x5,%edx` : Shiftea %edx por 5
* Las ultimas 4 operaciones son la cuenta  de setear buf = XMAX*2*(offset+linea)
* `add    $0xb8000,%edx` : Suma en %edx el valor de VGABUF original
* `movzbl (%eax),%ebx` :  Mueve el numero unsigned $eax a un double word y lo guarda en %ebx, que era *s
* `test   %bl,%bl` : testea que el ultimo byte de %ebx no sea 0 (*s)
* `je     10003f` : Si la condicion es 0, salta a la posicion 10003f, final del programa
* `add    $0x1,%eax` : Se suma 1 a %eax
* `mov    %bl,(%edx)` : Mueve el ultimo byte del registro %ebx a la posicion apuntada por %edx
* `mov    %cl,0x1(%edx)` : Mueve el ultimo byte del registro c a un byte de distancia de la posicion apuntada por %edx
* `movzbl (%eax),%ebx` : mueve extendiendo el numero unsigned que esta en al posicion apuntada por %eax a %ebx
* `lea    0x2(%edx),%edx` : Carga en $edx la posicion apuntada por %edx + 2 bytes
* `test   %bl,%bl` : testea que el ultimo byte de %ebx no sea 0 (*s)
* `jne    10002d` : Si es negativo la condicion del test, vuelve a 10002d, el principio del while
* `pop    %ebx` : Restaura el valor de %ebx
* `ret ` : Vuelve desde la llamada al codigo que la llamo
