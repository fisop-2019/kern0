#include <stdint.h>
#define VGABUF ((volatile char *) 0xb8000)
// static char const * VGABUF = (volatile char *) 0xb8000;

// volatile static unsigned *VGABUF = ((volatile unsigned *) 0xb8000);

static const char XMAX = 80;
static const char YMAX = 25;

static void vga_write(const char *s, int8_t linea, uint8_t color) { 
        volatile char *buf = VGABUF;
        if (linea < YMAX || linea > -YMAX){ 
            char offset = 0;
            if (linea < 0) {
                offset = YMAX; 
            }    
            buf += XMAX*2*(offset+linea);
            while (*s != 0)
            {
                *buf++ = *s++;
                *buf++ = color;
            }
        }
    }

void comienzo(void) {
    // VGABUF += 80;
    // *(VGABUF + 120) += 88;


    // volatile unsigned *p = VGABUF;
    // *p = 0x2F4B2F4F;

    //KERN0-ENDIAN
    // Versión 1
    // volatile uint64_t *p = VGABUF + 160;
    // *p = 0xE041E04cE04fE048;

    // Versión 2
    // volatile uint64_t *p = VGABUF;
    // p += 160;
    // *p = 0xE041E04cE04fE048;



    // *buf++ = 79;
    // *buf++ = 47;
    // *buf++ = 75;
    // *buf++ = 47;

    // volatile char * buf = VGABUF;
    char s[] = "OK";
    vga_write(s, 0, 47);
    char s2[] = "HOLA";
    vga_write(s2, 1, 224);
    while (1)
        asm("hlt");
}
