
# Comandos
## Borrar viejos
```bash
rm -f kern0.o boot.o kern0 
```

## compile: 
```bash
gcc -g -m32 -O1 -c kern0.c boot.S 
```

## link: 
```bash
 ld  -m elf_i386 --entry 0x100000 kern0.o boot.o -o kern0
```

## run qemu
```bash
qemu-system-i386 -serial mon:stdio -kernel kern0 
```

### in tmux
```bash
qemu-system-i386 -monitor stdio -kernel kern0 
```

## connect with gdb
```bash
gdb -q -s kern0 -n -ex 'target remote 127.0.0.1:7508'
```


## one liner
```bash
rm -f kern0.o boot.o kern0;
gcc -g -m32 -O1 -c kern0.c boot.S ;
ld  -m elf_i386 -Ttext 0x100000 --entry 0x100000 kern0.o boot.o -o kern0;
qemu-system-i386 -serial mon:stdio -kernel kern0 -gdb tcp:127.0.0.1:7508;
```
