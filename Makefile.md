```C
#FLAGS STANDARD CODIGO DE LA MATERIA
CFLAGS := -g -std=c99 -Wall -Wextra -Wpedantic

#FLAGS EXTRA PARA KERN0
CFLAGS += -m32 -O1 -fasm -ffreestanding -fno-inline

# CLFAGS += $(EXTRA_CFLAGS)

# CPPFLAGS := $(CFLAGS)

# CC := clang



SRCS := $(wildcard *.c)
OBJS := $(patsubst %.c,%.o,$(wildcard *.c))

kern0: boot.o $(OBJS)
	ld -m elf_i386 -Ttext 0x100000 --entry comienzo $^ -o $@
	objdump -S $@ >$@.asm
	# Verificar imagen Multiboot v1.
	grub-file --is-x86-multiboot $@

%.o: %.S
	$(CC) $(CFLAGS) -c $<

clean:
	rm -f kern0 kern0.asm *.o core

QEMU := qemu-system-i386 -monitor stdio
KERN := kern0
BOOT := -kernel $(KERN)

qemu: $(KERN)
	$(QEMU) $(BOOT)

qemu-gdb: $(KERN)
	$(QEMU) -kernel kern0 -S -gdb tcp:127.0.0.1:7508 $(BOOT)

gdb:
	gdb -q -s kern0 -n -ex 'target remote 127.0.0.1:7508'

a: clean qemu-gdb

.PHONY: qemu qemu-gdb gdb .clean r

end: end.c 
```